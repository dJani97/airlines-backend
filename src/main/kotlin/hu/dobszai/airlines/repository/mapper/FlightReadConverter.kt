package hu.dobszai.airlines.repository.mapper

import hu.dobszai.airlines.model.*
import io.r2dbc.spi.Row
import org.springframework.core.convert.converter.Converter
import org.springframework.data.convert.ReadingConverter
import java.time.Duration
import java.util.*
import kotlin.time.ExperimentalTime


@ReadingConverter
class FlightReadConverter : Converter<Row, Flight> {

  @ExperimentalTime
  override fun convert(r: Row): Flight {
    val sourceCity = mapCity(r, queryPrefixSource)
    val destinationCity = mapCity(r, queryPrefixDestination)
    val airline = mapAirline(r, queryPrefixAirline)
    return Flight(
        id = r.get(colFlightId, UUID::class.java)!!,
        source = sourceCity,
        destination =  destinationCity,
        airline = airline,
        duration = Duration.ofMinutes(r.get(colFlightDuration)!!.toString().toLong()),
        distance = r.get(colFlightDistance).toString().toInt()
    )
  }

  private fun mapCity(r: Row, prefix: String): City = City(
      id = r.get("${prefix}_${colCityId}", UUID::class.java)!!,
      name = r.get("${prefix}_${colCityName}", String::class.java)!!,
      population = r.get("${prefix}_${colCityPopulation}")!!.toString().toInt()
  )

  private fun mapAirline(r: Row, prefix: String): Airline = Airline(
      id = r.get("${prefix}_${colAirlineId}", UUID::class.java)!!,
      name = r.get("${prefix}_${colAirlineName}", String::class.java)!!,
  )
}
