package hu.dobszai.airlines.repository

import hu.dobszai.airlines.model.City
import kotlinx.coroutines.flow.Flow
import org.springframework.data.r2dbc.core.DatabaseClient
import org.springframework.data.r2dbc.core.flow
import org.springframework.data.relational.core.query.Criteria
import org.springframework.data.repository.kotlin.CoroutineCrudRepository
import org.springframework.stereotype.Component
import reactor.core.publisher.Mono
import java.util.*

interface CityRepository : CoroutineCrudRepository<City, UUID> {
  fun findFirstByOrderByPopulationDesc(): Mono<City>
  fun findFirstByOrderByPopulationAsc(): Mono<City>
}

@Component
class CityQueryRepository(
    val databaseClient: DatabaseClient) {

  fun query(id: UUID?, name: String?, population: Int?): Flow<City> {
    var criteria = Criteria.empty()
    id?.let { criteria = Criteria.from(criteria, Criteria.where("id").`is`(id)) }
    name?.let { criteria = Criteria.from(criteria, Criteria.where("name").like("%$name%")) }
    population?.let { criteria = Criteria.from(criteria, Criteria.where("population").`is`(population)) }
    return databaseClient.select()
        .from(City::class.java)
        .matching(criteria)
        .fetch()
        .flow()
  }
}
