package hu.dobszai.airlines.repository

import hu.dobszai.airlines.model.Airline
import org.springframework.data.repository.kotlin.CoroutineCrudRepository
import java.util.*

interface AirlineRepository : CoroutineCrudRepository<Airline, UUID>
