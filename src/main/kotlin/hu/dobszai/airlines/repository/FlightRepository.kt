package hu.dobszai.airlines.repository

import hu.dobszai.airlines.model.Flight
import kotlinx.coroutines.flow.Flow
import org.springframework.data.r2dbc.repository.Query
import org.springframework.data.repository.kotlin.CoroutineCrudRepository
import org.springframework.data.repository.query.Param
import java.util.*

interface FlightRepository : CoroutineCrudRepository<Flight, UUID> {
  @Query("select f.id, f.source_id, f.destination_id, f.duration_minutes, f.distance, " +
      "cs.name as source_name, cs.population as source_population, " +
      "cd.name as destination_name, cd.population as destination_population, " +
      "a.id as airline_id, a.name as airline_name " +
      "from flights f join cities cs on f.source_id = cs.id join cities cd on f.destination_id = cd.id " +
      "join airlines a on f.airline_id = a.id " +
      "where :airlineId is null or f.airline_id = :airlineId")
  fun query(@Param("airlineId") airlineId: UUID? = null): Flow<Flight>
}
