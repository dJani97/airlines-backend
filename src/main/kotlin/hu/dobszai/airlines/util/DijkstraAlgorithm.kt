package hu.dobszai.airlines.util

import hu.dobszai.airlines.model.interfaces.Edge
import hu.dobszai.airlines.model.interfaces.Vertex

/**
 * A reimplementation of Dijkstra’s algorithm based on [Vogella's Tutorial][1]
 *
 * [1]: http://www.vogella.com/tutorials/JavaAlgorithmsDijkstra/article.html
 */

class DijkstraAlgorithm<V : Vertex, E : Edge<V>>(edges: List<E>) {
  private val edges = edges.toMutableList()
  private val settledNodes = mutableSetOf<V>()
  private val unsettledNodes = mutableSetOf<V>()
  private val distance = mutableMapOf<V, Int>()
  private val predecessors = mutableMapOf<V, V>()

  /**
   * Runs the calculation from [source] to all destinations.
   * The algorithm should be executed before calling [getPath].
   */
  fun execute(source: V) {
    settledNodes.clear()
    unsettledNodes.clear()
    distance.clear()
    predecessors.clear()
    distance[source] = 0
    unsettledNodes.add(source)
    while (unsettledNodes.size > 0) {
      getMinimum(unsettledNodes)?.let { node ->
        settledNodes.add(node)
        unsettledNodes.remove(node)
        findMinimalDistances(node)
      }
    }
  }

  private fun findMinimalDistances(node: V) {
    val adjacentNodes = getNeighbors(node)
    adjacentNodes.forEach { target ->
      if (getShortestDistance(target) > getShortestDistance(node) + getDistance(node, target)) {
        distance[target] = (getShortestDistance(node) + getDistance(node, target))
        predecessors[target] = node
        unsettledNodes.add(target)
      }
    }
  }

  private fun getDistance(node: V, target: V): Int = edges
      .firstOrNull { it.source == node && it.destination == target }
      ?.weight ?: Int.MAX_VALUE

  private fun getNeighbors(node: V): List<V> = edges
      .filter { it.source == node && !isSettled(it.destination) }
      .map { it.destination }

  private fun getMinimum(vertices: Set<V>): V? = vertices.minByOrNull { getShortestDistance(it) }

  private fun isSettled(vertex: V) = settledNodes.contains(vertex)

  private fun getShortestDistance(destination: V) = distance[destination] ?: Int.MAX_VALUE

  /**
   * Return a list of vertices if the path to [target] exists
   * Return null if there is no possible way to get to [target]
   */
  fun getPath(target: V): List<V>? {
    val path = mutableListOf<V>()
    var currentStep = target

    // check if a path exists
    if (!predecessors.containsKey(currentStep))
      return null

    path.add(currentStep)
    while (predecessors.containsKey(currentStep)) {
      currentStep = predecessors[currentStep]!!
      path.add(currentStep)
    }
    // Put it into the correct order
    path.reverse()
    return path
  }

  /**
   * Return a list of edges if the path to [target] exists
   * Return null if there is no possible way to get to [target]
   */
  fun getPathEdges(target: V): List<E>? {
    val path = getPath(target) ?: return null
    val pathEdges = mutableListOf<E>()
    path.zipWithNext { a, b ->
      edges.filter { it.source == a && it.destination == b }
          .minByOrNull { it.weight }
          ?.let { pathEdges.add(it) }
    }
    return pathEdges
  }

}
