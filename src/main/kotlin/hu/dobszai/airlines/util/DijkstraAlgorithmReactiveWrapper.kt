package hu.dobszai.airlines.util

import hu.dobszai.airlines.model.City
import hu.dobszai.airlines.model.Flight
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.reactive.asFlow
import kotlinx.coroutines.withContext
import reactor.kotlin.core.publisher.toFlux

@JvmName("shortestPath1")
fun shortestPath(edges: Iterable<Flight>, from: City, to: City): Flow<Flight> {
  val algorithm = DijkstraAlgorithm(edges.toList())
  algorithm.execute(from)
  val path = algorithm.getPathEdges(to)
      ?: error("No path exists")
  return path.toFlux().asFlow()
}

fun Iterable<Flight>.shortestPath(from: City, to: City): Flow<Flight>
    = shortestPath(this, from, to)

suspend fun Flow<Flight>.shortestPath(from: Flow<City>, to: Flow<City>): Flow<Flight> {
  val flow = this
  return withContext(Dispatchers.Default) { flow.toList().shortestPath(from.first(), to.first()) }
}
