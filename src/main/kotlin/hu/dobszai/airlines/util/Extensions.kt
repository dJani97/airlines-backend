package hu.dobszai.airlines.util

import java.time.Duration
import java.util.*

fun String.toUUID(): UUID? = UUID.fromString(this)

fun Duration.roundUpToHour(): Duration {
  val differenceToNextHour = Duration.ofHours(1)
      .minus(Duration.ofMinutes(this.toMinutesPart().toLong()))
  return this.plus(differenceToNextHour)
}
