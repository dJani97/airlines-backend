package hu.dobszai.airlines.model

import com.fasterxml.jackson.annotation.JsonPropertyOrder
import hu.dobszai.airlines.util.roundUpToHour
import java.time.Duration

@JsonPropertyOrder("flights", "totalDuration", "totalDurationMinutes")
class FlightPlan(val flights: List<Flight>) {

  val totalDuration get(): Duration {
    var duration = Duration.ZERO
    flights.forEachIndexed { index, flight ->
      duration = when {
        index < flights.size - 1 -> duration.plus(flight.duration.roundUpToHour())
        else -> duration.plus(flight.duration)
      }
    }
    return duration
  }

  val totalDurationMinutes get() = totalDuration.toMinutes()

}
