package hu.dobszai.airlines.model

import hu.dobszai.airlines.model.interfaces.Identifiable
import hu.dobszai.airlines.model.interfaces.Vertex
import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import java.util.*


const val colCityId = "id"
const val colCityName = "name"
const val colCityPopulation = "population"

@Table("cities")
data class City(

    @Id @Column(value = colCityId)
    override val id: UUID,

    @Column(value = colCityName)
    val name: String,

    @Column(value = colCityPopulation)
    val population: Int = 0

) : Vertex, Identifiable<UUID>
