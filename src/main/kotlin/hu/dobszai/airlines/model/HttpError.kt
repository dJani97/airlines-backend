package hu.dobszai.airlines.model

import com.fasterxml.jackson.annotation.JsonInclude

@JsonInclude(JsonInclude.Include.NON_NULL)
class HttpError(
    val message: String,
    val details: String? = null,
    val statusCode: Int? = null
)
