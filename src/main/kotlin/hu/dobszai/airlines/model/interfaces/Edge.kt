package hu.dobszai.airlines.model.interfaces

interface Edge<V : Vertex> {
  val destination: V
  val source: Vertex
  val weight: Int
}
