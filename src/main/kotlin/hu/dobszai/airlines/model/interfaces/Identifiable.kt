package hu.dobszai.airlines.model.interfaces

import java.io.Serializable


interface Identifiable<ID : Serializable> {
  val id: ID
}
