package hu.dobszai.airlines.model.interfaces

import java.util.*

interface Vertex {
  val id: UUID
}
