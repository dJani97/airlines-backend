package hu.dobszai.airlines.model

import hu.dobszai.airlines.model.interfaces.Identifiable
import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import java.util.*


const val colAirlineId = "id"
const val colAirlineName = "name"

@Table("airlines")
data class Airline(

    @Id
    @Column(colAirlineId)
    override val id: UUID,

    @Column(colAirlineName)
    val name: String

) : Identifiable<UUID>
