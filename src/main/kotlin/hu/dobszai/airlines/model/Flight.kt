package hu.dobszai.airlines.model

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonPropertyOrder
import hu.dobszai.airlines.model.interfaces.Edge
import hu.dobszai.airlines.model.interfaces.Identifiable
import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import java.time.Duration
import java.util.*


const val colFlightId = "id"
const val colFlightDuration = "duration_minutes"
const val colFlightDistance = "distance"

const val queryPrefixSource = "source"
const val queryPrefixDestination = "destination"
const val queryPrefixAirline = "airline"

@Table("flights")
@JsonPropertyOrder("id", "source", "destination", "airline", "duration", "durationMinutes", "distance")
data class Flight(

    @Id @Column(colFlightId)
    override val id: UUID,

    override val source: City,
    override val destination: City,
    val airline: Airline,

    @Column(colFlightDuration)
    val duration: Duration,

    @Column(colFlightDistance)
    val distance: Int

) : Edge<City>, Identifiable<UUID> {

   @get:JsonIgnore
   override val weight: Int get() = distance

   val durationMinutes: Long get() = duration.toMinutes()
}
