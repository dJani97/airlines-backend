package hu.dobszai.airlines.handler

import hu.dobszai.airlines.service.AirlineService
import org.springframework.http.MediaType
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import org.springframework.web.reactive.function.server.bodyAndAwait

@Component
class AirlineHandler(private val service: AirlineService) {

  suspend fun findAll(request: ServerRequest): ServerResponse = ServerResponse.ok()
      .contentType(MediaType.APPLICATION_JSON)
      .bodyAndAwait(service.findAll())
}
