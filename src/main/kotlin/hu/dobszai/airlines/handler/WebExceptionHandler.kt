package hu.dobszai.airlines.handler

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.ObjectMapper
import hu.dobszai.airlines.model.HttpError
import org.springframework.boot.web.reactive.error.ErrorWebExceptionHandler
import org.springframework.context.annotation.Configuration
import org.springframework.core.annotation.Order
import org.springframework.core.io.buffer.DataBuffer
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.web.server.ResponseStatusException
import org.springframework.web.server.ServerWebExchange
import reactor.core.publisher.Mono


@Configuration
@Order(-2)
class WebExceptionHandler(val objectMapper: ObjectMapper) : ErrorWebExceptionHandler {
  override fun handle(serverWebExchange: ServerWebExchange, throwable: Throwable): Mono<Void> {

    if (throwable is ResponseStatusException) {
      return serverWebExchange.writeObject(HttpError(
          message = throwable.reason ?: throwable.status.toString(),
          statusCode = throwable.status.value()
      ))
    }

    serverWebExchange.response.statusCode = HttpStatus.INTERNAL_SERVER_ERROR
    return serverWebExchange.writeObject(HttpError(
        "Unknown error",
        throwable.message,
        HttpStatus.INTERNAL_SERVER_ERROR.value()
    ))
  }

  fun ServerWebExchange.writeObject(httpError: HttpError): Mono<Void> {
    val bufferFactory = response.bufferFactory()
    val dataBuffer: DataBuffer = try {
      bufferFactory.wrap(objectMapper.writeValueAsBytes(
          httpError))
    } catch (e: JsonProcessingException) {
      bufferFactory.wrap("".toByteArray())
    }
    response.headers.contentType = MediaType.APPLICATION_JSON
    return response.writeWith(Mono.just(dataBuffer))
  }
}

