package hu.dobszai.airlines.handler

import hu.dobszai.airlines.service.FlightPlanService
import hu.dobszai.airlines.util.toUUID
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.*
import org.springframework.web.server.ResponseStatusException

@Component
class FlightPlanHandler(private val service: FlightPlanService) {

  suspend fun findShortestPath(request: ServerRequest): ServerResponse {
    val airline = request.queryParamOrNull("airline")?.toUUID()
    val source = request.queryParamOrNull("source")?.toUUID()
    val destination = request.queryParamOrNull("destination")?.toUUID()

    return if (source != null && destination != null)
      ServerResponse.ok()
          .contentType(MediaType.APPLICATION_JSON)
          .bodyValueAndAwait(service.findShortestPath(airline, source, destination))

    else
      throw ResponseStatusException(HttpStatus.BAD_REQUEST, "source and destination parameters are required")
  }
}
