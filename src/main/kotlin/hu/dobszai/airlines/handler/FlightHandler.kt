package hu.dobszai.airlines.handler

import hu.dobszai.airlines.service.FlightService
import hu.dobszai.airlines.util.toUUID
import org.springframework.http.MediaType
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import org.springframework.web.reactive.function.server.bodyAndAwait
import org.springframework.web.reactive.function.server.queryParamOrNull

@Component
class FlightHandler(private val service: FlightService) {

  suspend fun findAll(request: ServerRequest): ServerResponse = ServerResponse.ok()
      .contentType(MediaType.APPLICATION_JSON)
      .bodyAndAwait(service.query(
          request.queryParamOrNull("airline")?.toUUID()
      ))
}
