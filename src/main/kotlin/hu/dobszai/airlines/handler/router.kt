package hu.dobszai.airlines.handler

import kotlinx.coroutines.flow.flowOf
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.MediaType.APPLICATION_JSON
import org.springframework.web.reactive.function.server.bodyAndAwait
import org.springframework.web.reactive.function.server.coRouter

@Configuration
class RouterConfig(val cityHandler: CityHandler,
                   val flightHandler: FlightHandler,
                   val airlineHandler: AirlineHandler,
                   val flightPlanHandler: FlightPlanHandler) {

  @Bean
  fun router() = coRouter {
    GET("/") { ok().bodyAndAwait(flowOf("Hello There")) }
    accept(APPLICATION_JSON).nest {
      "/city".nest {
        GET("", cityHandler::findAll)
        GET("/largest", cityHandler::findLargest)
        GET("/smallest", cityHandler::findSmallest)
      }
      GET("/flight", flightHandler::findAll)
      GET("/airline", airlineHandler::findAll)
      GET("/flight-plan", flightPlanHandler::findShortestPath)
    }
  }
}

