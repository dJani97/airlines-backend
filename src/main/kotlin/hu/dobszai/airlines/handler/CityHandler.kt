package hu.dobszai.airlines.handler

import hu.dobszai.airlines.service.CityService
import hu.dobszai.airlines.util.toUUID
import org.springframework.http.MediaType
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.*

@Component
class CityHandler(private val service: CityService) {

  suspend fun findAll(request: ServerRequest): ServerResponse = ServerResponse.ok()
      .contentType(MediaType.APPLICATION_JSON)
      .bodyAndAwait(service.query(
          request.queryParamOrNull("id")?.toUUID(),
          request.queryParamOrNull("name"),
          request.queryParamOrNull("population")?.toInt()
      ))

  suspend fun findLargest(request: ServerRequest): ServerResponse = ServerResponse.ok()
      .contentType(MediaType.APPLICATION_JSON)
      .bodyValueAndAwait(service.findLargest())

  suspend fun findSmallest(request: ServerRequest): ServerResponse = ServerResponse.ok()
      .contentType(MediaType.APPLICATION_JSON)
      .bodyValueAndAwait(service.findSmallest())
}
