package hu.dobszai.airlines

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class AirlinesApplication

fun main(args: Array<String>) {
  runApplication<AirlinesApplication>(*args)
}
