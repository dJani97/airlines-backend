package hu.dobszai.airlines.config

import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component

@Component
class AppProperties(

    @Value("\$spring.r2dbc.url")
    val r2dbcUrl: String
)
