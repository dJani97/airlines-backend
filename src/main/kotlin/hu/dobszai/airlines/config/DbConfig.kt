package hu.dobszai.airlines.config

import hu.dobszai.airlines.repository.mapper.FlightReadConverter
import io.r2dbc.spi.ConnectionFactories
import io.r2dbc.spi.ConnectionFactory
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.convert.converter.Converter
import org.springframework.core.io.ClassPathResource
import org.springframework.data.r2dbc.config.AbstractR2dbcConfiguration
import org.springframework.data.r2dbc.connectionfactory.init.CompositeDatabasePopulator
import org.springframework.data.r2dbc.connectionfactory.init.ConnectionFactoryInitializer
import org.springframework.data.r2dbc.connectionfactory.init.ResourceDatabasePopulator
import java.util.*


@Configuration
class DbConfig(val appProperties: AppProperties) : AbstractR2dbcConfiguration() {

  companion object {
    // TODO move this to props OR find a way to detect if DB is initialized
    const val initializeDB = true;
  }

  @Bean
  fun initializer(@Qualifier("connectionFactory") connectionFactory: ConnectionFactory): ConnectionFactoryInitializer {
    val initializer = ConnectionFactoryInitializer()
    initializer.setConnectionFactory(connectionFactory)
    if (initializeDB) {
      val populator = CompositeDatabasePopulator()
      populator.addPopulators(ResourceDatabasePopulator(ClassPathResource("schema.sql")))
      populator.addPopulators(ResourceDatabasePopulator(ClassPathResource("helper.sql")))
      populator.addPopulators(ResourceDatabasePopulator(ClassPathResource("data.sql")))
      initializer.setDatabasePopulator(populator)
    }
    return initializer
  }

  override fun getCustomConverters(): List<Any> {
    val converterList = ArrayList<Converter<*, *>>()
    converterList.add(FlightReadConverter())
    return converterList
  }

  override fun connectionFactory(): ConnectionFactory
      = ConnectionFactories.get(appProperties.r2dbcUrl)

}
