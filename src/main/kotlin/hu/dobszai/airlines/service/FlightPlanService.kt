package hu.dobszai.airlines.service

import hu.dobszai.airlines.model.FlightPlan
import hu.dobszai.airlines.util.shortestPath
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.reactive.asFlow
import kotlinx.coroutines.reactor.asFlux
import org.springframework.stereotype.Service
import java.util.*

@Service
class FlightPlanService(val flightService: FlightService,
                        val cityService: CityService) {

  suspend fun findShortestPath(airlineId: UUID? = null, source: UUID, destination: UUID): FlightPlan {
    val flights = flightService.query(airlineId)
    val sourceCity = cityService.query(id = source)
    val destinationCity = cityService.query(id = destination)

    return FlightPlan(flights.shortestPath(sourceCity, destinationCity).asFlux().checkpoint("1").asFlow().toList())
  }
}
