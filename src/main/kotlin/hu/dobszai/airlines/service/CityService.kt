package hu.dobszai.airlines.service

import hu.dobszai.airlines.model.City
import hu.dobszai.airlines.repository.CityQueryRepository
import hu.dobszai.airlines.repository.CityRepository
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.reactive.asFlow
import org.springframework.stereotype.Service
import java.util.*

@Service
class CityService(val repository: CityRepository,
                  val queryRepository: CityQueryRepository) {

  suspend fun findAll() = repository.findAll()

  suspend fun findLargest(): City = repository.findFirstByOrderByPopulationDesc().asFlow().first()

  suspend fun findSmallest(): City = repository.findFirstByOrderByPopulationAsc().asFlow().first()

  fun query(id: UUID? = null, name: String? = null, population: Int? = null)
      = queryRepository.query(id, name, population)
}
