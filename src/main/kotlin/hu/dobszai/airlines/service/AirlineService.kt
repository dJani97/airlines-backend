package hu.dobszai.airlines.service

import hu.dobszai.airlines.repository.AirlineRepository
import org.springframework.stereotype.Service

@Service
class AirlineService(val airlineRepository: AirlineRepository) {

  fun findAll() = airlineRepository.findAll()
}
