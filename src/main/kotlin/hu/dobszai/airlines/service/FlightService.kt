package hu.dobszai.airlines.service

import hu.dobszai.airlines.repository.FlightRepository
import org.springframework.stereotype.Service
import java.util.*

@Service
class FlightService(val repository: FlightRepository) {

  fun findAll() = query()

  fun query(airlineId: UUID? = null) = repository.query(airlineId)
}
