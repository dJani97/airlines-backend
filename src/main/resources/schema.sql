CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

drop table if exists flights;
drop table if exists airlines;
drop table if exists cities;

create table cities (
    id uuid primary key,
    name text,
    population integer
);

create table airlines (
    id uuid primary key,
    name text
);


create table flights (
    id uuid primary key,
    source_id uuid references cities,
    destination_id uuid references cities,
    airline_id uuid references airlines,
    duration_minutes integer,
    distance integer
);
