-- helpers

CREATE OR REPLACE FUNCTION get_city_id_of(_name text) RETURNS UUID AS '
    BEGIN
        RETURN (select c.id from cities c where c.name = _name limit 1);
    END;
' LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION get_airline_id_of(_name text) RETURNS UUID AS '
    BEGIN
        RETURN (select a.id from airlines a where a.name = _name limit 1);
    END;
' LANGUAGE plpgsql;
