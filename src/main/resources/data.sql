-- cities

insert into cities (id, name, population) VALUES
    ('37d68d46-d476-4b34-b3aa-b4ef97a01af9', 'Pécs',     145011),
    ('7c906fea-2533-40b5-9753-8d81ddc9793b', 'Budapest', 1752000),
    ('9b4570b5-affd-47fa-9b5b-89da744e1ee3', 'London',   9000000),
    ('6693548d-fc0e-466e-ae20-40ca38b3de9d', 'Berlin',   3460000),
    ('a28208f9-687a-4def-8066-dc1bcac663de', 'Madrid',   3198000),
    ('a8da1bf0-cda8-4723-b44a-37f838efa8ae', 'Kiev',     3000000),
    ('725d0376-9ee6-4f68-a6df-b2c1b27fdc6f', 'Rome',     2844750),
    ('66040983-6392-4d9b-933a-23cfcd316347', 'Paris',    2140000),
    ('ec052661-2d18-46fa-a245-b7342d90e330', 'Warsaw',   1856789),
    ('e1fbf9ab-dd42-4f30-bfc6-faeb57bdad8c', 'Amsterdam', 821752),
    ('31a52e75-945a-46de-92d3-e9cba5506c87', 'The Hague', 544766),
    ('58e9d51f-89c1-4786-84ba-16bd1d5fd432', 'Vinnytsia', 372484);


-- airlines

insert into airlines (id, name) VALUES
   (uuid_generate_v4(), 'WizzAird'),
   (uuid_generate_v4(), 'InnoAir'),
   (uuid_generate_v4(), 'SuAir'),
   (uuid_generate_v4(), 'AirStar'),
   (uuid_generate_v4(), 'AirZilla');


-- flights

insert into flights (id, source_id, destination_id, airline_id, duration_minutes, distance) VALUES
    (uuid_generate_v4(), get_city_id_of('Pécs'),      get_city_id_of('Budapest'),  get_airline_id_of('InnoAir'),  30,  200),
    (uuid_generate_v4(), get_city_id_of('Pécs'),      get_city_id_of('Budapest'),  get_airline_id_of('WizzAird'), 40,  199),
    (uuid_generate_v4(), get_city_id_of('Budapest'),  get_city_id_of('Pécs'),      get_airline_id_of('WizzAird'), 40,  199),
    (uuid_generate_v4(), get_city_id_of('Budapest'),  get_city_id_of('London'),    get_airline_id_of('InnoAir'),  150, 1449),
    (uuid_generate_v4(), get_city_id_of('Budapest'),  get_city_id_of('Berlin'),    get_airline_id_of('InnoAir'),  75,  685),
    (uuid_generate_v4(), get_city_id_of('Berlin'),    get_city_id_of('Budapest'),  get_airline_id_of('InnoAir'),  75,  685),
    (uuid_generate_v4(), get_city_id_of('Berlin'),    get_city_id_of('Kiev'),      get_airline_id_of('InnoAir'),  101, 1192),
    (uuid_generate_v4(), get_city_id_of('London'),    get_city_id_of('Berlin'),    get_airline_id_of('InnoAir'),  100, 963),
    (uuid_generate_v4(), get_city_id_of('Kiev'),      get_city_id_of('Rome'),      get_airline_id_of('SuAir'),    157, 1690),
    (uuid_generate_v4(), get_city_id_of('Kiev'),      get_city_id_of('Paris'),     get_airline_id_of('InnoAir'),  198, 2000),
    (uuid_generate_v4(), get_city_id_of('Paris'),     get_city_id_of('Warsaw'),    get_airline_id_of('InnoAir'),  108, 1368),
    (uuid_generate_v4(), get_city_id_of('Rome'),      get_city_id_of('Warsaw'),    get_airline_id_of('SuAir'),    113, 1326);
