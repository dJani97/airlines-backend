package hu.dobszai.airlines.util

import hu.dobszai.airlines.flights
import hu.dobszai.airlines.cities
import kotlinx.coroutines.reactor.asFlux
import org.junit.jupiter.api.Test
import reactor.test.StepVerifier

class ShortestPathWrapperTest {

  @Test
  fun `When using the DijkstraAlgorithmReactiveWrapper, no error is thrown`() {
    // give: edges

    // when
    // TODO: make this test entirely reactive
    val flowOfPath = flights
        .shortestPath(from = cities[0], to = cities[2])

    // then
    StepVerifier.create(flowOfPath.asFlux())
        .expectNextCount(4)
        .expectComplete()
        .verify()
  }
}
