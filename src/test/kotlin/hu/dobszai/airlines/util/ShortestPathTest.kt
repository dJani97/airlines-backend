package hu.dobszai.airlines.util

import hu.dobszai.airlines.flights
import hu.dobszai.airlines.cities
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions.*

class ShortestPathTest {

  @Test
  fun `When running executing DijkstraAlgorithm, the results are not null`() {
    // given
    val algorithm = DijkstraAlgorithm(flights)

    // when
    algorithm.execute(cities[0]);
    val path = algorithm.getPath(cities[2])

    // then
    print(path)
    assertNotNull(path)
    assertTrue(path?.size == 4)
  }
}
