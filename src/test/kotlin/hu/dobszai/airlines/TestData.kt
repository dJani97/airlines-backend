package hu.dobszai.airlines

import hu.dobszai.airlines.model.Airline
import hu.dobszai.airlines.model.City
import hu.dobszai.airlines.model.Flight
import java.time.Duration
import java.util.*

val airlines: List<Airline> = listOf(
    Airline(UUID.randomUUID(), "A"),
    Airline(UUID.randomUUID(), "B"))

val cities: List<City> = listOf(
    City(UUID.randomUUID(), "A"),
    City(UUID.randomUUID(), "B"),
    City(UUID.randomUUID(), "C"),
    City(UUID.randomUUID(), "D"),
    City(UUID.randomUUID(), "E"))

val flights: List<Flight> = listOf(
    Flight(UUID.randomUUID(), cities[0], cities[3], airlines[0], Duration.ofMinutes(10), 1),
    Flight(UUID.randomUUID(), cities[0], cities[1], airlines[0], Duration.ofMinutes(60), 6),
    Flight(UUID.randomUUID(), cities[3], cities[1], airlines[0], Duration.ofMinutes(20), 2),
    Flight(UUID.randomUUID(), cities[1], cities[4], airlines[0], Duration.ofMinutes(20), 2),
    Flight(UUID.randomUUID(), cities[3], cities[4], airlines[0], Duration.ofMinutes(10), 1),
    Flight(UUID.randomUUID(), cities[4], cities[2], airlines[0], Duration.ofMinutes(50), 5),
    Flight(UUID.randomUUID(), cities[1], cities[2], airlines[0], Duration.ofMinutes(50), 5),
)
