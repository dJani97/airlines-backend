# INNOlines REST service

A backend application for the INNOlines demo project

The service  
 - provides a REST API to query airlines, cities and flights  
 - provides an endpoint to find the best path of flights between two cities

## Installation Instructions

### Cloning this repository

```
git clone git@bitbucket.org:dJani97/airlines-backend.git
```

### Running the project

#### Start the dependencies

This project depends on a DBMS. This dependency is provided as Docker container, and can be started by executing the following script:
  
##### Run PostgreSQL Server

```
./bin/run_db.sh
```

#### Start the application

After starting the database, the service can be built and started with the following script:

```
./bin/run.sh
```
